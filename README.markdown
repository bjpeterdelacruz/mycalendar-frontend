# myCalendar

## Client Side Implementation

### About

This repository contains the client side (frontend) implementation
of myCalendar.

### Links

A public web version of myCalendar can be found [here](http://www.bjpeterdelacruz.com/calendar/index.html).

