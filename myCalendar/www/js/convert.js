        function onChange() {
            $('#to option').remove();
            $('#to').append('<option value="Gregorian">Gregorian</option>');
            $('#to').append('<option value="Hijri">Hijri</option>');
            $('#to').append('<option value="Jalaali">Jalaali</option>');

            if ($('#from option:selected').text() == "Gregorian") {
                $("#to option[value='Gregorian']").remove();
            } else if ($('#from option:selected').text() == "Hijri") {
                $("#to option[value='Hijri']").remove();
            } else if ($('#from option:selected').text() == "Jalaali") {
                $("#to option[value='Jalaali']").remove();
            }

            enableConvertButton();
            $('#toDate').val('');
            $('#save').prop("disabled", true);
        }
        function enableConvertButton() {
            $('#convert').prop("disabled", !$('#fromDate').val());
        }
        function convert() {
            if ($('#from option:selected').text() == "Gregorian") {
                var date = new Date($('#fromDate').val());
                if ($('#to option:selected').text() == "Hijri") {
                    $('#toDate').val(moment(date).format('iMM/iDD/iYYYY'));
                } else if ($('#to option:selected').text() == "Jalaali") {
                    var m = moment(date);
                    var jalaali = toJalaali(m.year(), m.month(), m.date());
                    $('#toDate').val((jalaali.jm + 1) + "/" + jalaali.jd + "/" + jalaali.jy);
                }
            } else if ($('#from option:selected').text() == "Hijri") {
                if ($('#to option:selected').text() == "Gregorian") {
                    $('#toDate').val(moment($('#fromDate').val(), 'iMM/iDD/iYYYY').format('MM/DD/YYYY'));
                } else if ($('#to option:selected').text() == "Jalaali") {
                    // First convert to Gregorian, then to Jalaali
                    var value = moment($('#fromDate').val(), 'iMM/iDD/iYYYY').format('MM/DD/YYYY');
                    var m = moment(new Date(value));
                    var jalaali = toJalaali(m.year(), m.month(), m.date());
                    $('#toDate').val((jalaali.jm + 1) + "/" + jalaali.jd + "/" + jalaali.jy);
                }
            } else if ($('#from option:selected').text() == "Jalaali") {
                var array = $('#fromDate').val().split('/');
                var gregorian = toGregorian(parseInt(array[2]), parseInt(array[0]), parseInt(array[1]));
                if ($('#to option:selected').text() == "Gregorian") {
                    $('#toDate').val(gregorian.gm + "/" + gregorian.gd + "/" + gregorian.gy);
                } else if ($('#to option:selected').text() == "Hijri") {
                    var value = moment(gregorian.gm + "/" + gregorian.gd + "/" + gregorian.gy, 'YYYY-MM-DD').format('iMM/iDD/iYYYY');
                    $('#toDate').val(value);
                }
            }
            $('#save').prop("disabled", false);
        }
        function getUsername() {
            var username = store.get('username');
            console.log(username);
            return username;
        }
        function save() {
            $.ajax({
                url: 'http://sleepy-badlands-60233.herokuapp.com/conversions/' + getUsername(),
                type: 'PUT',
                contentType: 'application/json',
                data: buildJsonString(),
                success: function(result) {
                  populateConversionsList();
                }
            });
            console.log(buildJsonString());
        }
        function deleteAll() {
            if (confirm('Are you sure you want to delete all your saved conversions?')) {
                $.ajax({
                    url: 'http://sleepy-badlands-60233.herokuapp.com/conversions/' + getUsername(),
                    type: 'DELETE',
                    success: function(result) {
                        $('#conversions').clone().empty().insertAfter('#conversions').prev().remove();
                        $('#deleteAll').prop("disabled", "true");
                    }
                });
            }
        }
        function displayConversion(fromCalendar, fromDate, toCalendar, toDate) {
            $('#fromDate').val(fromDate);
            $("#from").val(fromCalendar).change();
            $("#to").val(toCalendar);
            $('#toDate').val(toDate);
            $('#convert').prop("disabled", true);
            $('#save').prop("disabled", true);
        }
        function buildJsonString() {
          return '{"deviceID":"' + getUsername() + '","conversions":[{'
            + "\"from\":\"" + $('#from').val() + "\","
            + "\"fromDate\":\"" + $('#fromDate').val() + "\","
            + "\"to\":\"" + $("#to").val() + "\","
            + "\"toDate\":\"" + $('#toDate').val() + "\""
            + "}]}"
        }
        function populateConversionsList() {
          $.getJSON("http://sleepy-badlands-60233.herokuapp.com/conversions/" + getUsername(), function(data) {
              $('#conversions').empty();
              $.each(data, function(index, element) {
                  var li = "<a href='#' id='conversion-" + (index + 1) + "' class='input-lg list-group-item list-group-item-action' onclick='displayConversion(\"" + element.from + "\",\"" + element.fromDate + "\",\"" + element.to + "\",\"" + element.toDate + "\");'>" + moment(new Date(element.fromDate)).format("MM/DD/YYYY") + "</a>";
                  $('#conversions').append(li);
                  $('#deleteAll').prop("disabled", false);
              });
          });
        }
