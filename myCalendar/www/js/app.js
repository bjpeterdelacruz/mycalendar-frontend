angular.module('calendarDemoApp', ['ionic', 'ngAnimate', 'ui.rCalendar', 'ngCordova', 'ngStorage'])
    .run(function ($ionicPlatform, $animate) {
        'use strict';
        $animate.enabled(false);
    })
    .config(function ($stateProvider, $urlRouterProvider) {
        'use strict';
        $stateProvider
            .state('tabs', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })
            .state('tabs.home', {
                url: '/home',
                views: {
                    'home-tab': {
                        templateUrl: 'templates/home.html',
                        controller: 'CalendarCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/tab/home');
    })
    .controller('CalendarCtrl', function ($scope, $http, $ionicPopup, $timeout, $ionicPlatform, $cordovaDevice, $cordovaLocalNotification, $localStorage) {
        'use strict';
        $scope.calendar = {};
        var deviceID;
        var events = [];
        var listPopup;
        var eventsName;
        var addPopup;
        var removePopup;
        var filterPopup;
        var popupList = [];
        var notificationId = 1;
        var notificationIds = {};

        $scope.events = [];

        $ionicPlatform.ready(function() {
            deviceID = ionic.Platform.isAndroid() ? $cordovaDevice.getUUID() : "samsung-galaxy-s6-edge";
            console.log(deviceID);
            if ($localStorage.events) {
              events = $localStorage.events;
              $timeout(function() {
                  $scope.calendar.eventSource = events;
              }, 500);
              $scope.events = events;
            }
        });

        $scope.changeMode = function (mode) {
            $scope.calendar.mode = mode;
        };

        $scope.showAddEventPopup = function() {
            addPopup = $ionicPopup.show({
                templateUrl: 'addTemplate.html',
                title: 'Add Event',
                scope: $scope
            });
        };

        $scope.showRemoveEventsPopup = function() {
          if ($scope.events.length == 0) {
            $ionicPopup.show({
                template: '<span style="font-weight: bold">You have no events in your calendar.</span>',
                title: 'No Events',
                scope: $scope,
                buttons: [{ text: 'Close' }]
              });
            return;
          }
          $scope.events.sort(function(a, b) {
            return a.startTime - b.startTime;
          });
          removePopup = $ionicPopup.show({
              templateUrl: 'removeTemplate.html',
              title: 'Remove Events',
              scope: $scope
          });
        }

        $scope.closeRemoveEventsPopup = function() {
          removePopup.close();
        }

        function getTime(time) {
            return time.getHours() + ":" + time.getMinutes();
        }

        $scope.addEvent = function(title, startDate, startTime, endDate, endTime, sendNotification) {
            var start = new Date(startDate + " " + getTime(startTime));
            var end = new Date(endDate + " " + getTime(endTime));
            $scope.calendar.eventSource = [];
            events.unshift({
                title: title,
                startTime: start.getTime(),
                endTime: end.getTime(),
                allDay: false
            });
            $timeout(function() {
                $scope.calendar.eventSource = events;
            }, 500);
            if (sendNotification) {
              notificationIds[title] = notificationId;
              cordova.plugins.notification.local.schedule({
                  id: notificationId,
                  text: title,
                  at: start.getTime() - 3600000,
                  led: "FF0000",
                  sound: 'file://res/sounds/bell.wav'
              });
              notificationId++;
            }
            $scope.events = events;
            $localStorage.events = events;
            $scope.closeAddEventPopup();
        };

        $scope.closeAddEventPopup = function() {
            addPopup.close();
        }

        $scope.showFilterEventsPopup = function() {
            filterPopup = $ionicPopup.show({
                templateUrl: 'filterTemplate.html',
                title: 'Filter Events',
                scope: $scope
            });
        };

        $scope.filterEvents = function(filter) {
            var temp = [];
            if (filter === "all") {
              temp = events;
            }
            else {
              for (var i = 0; i < events.length; i++) {
                console.log(events[i]);
                if (events[i].class === filter) {
                  temp.unshift(events[i]);
                } else if (!events[i].class && filter === "events") {
                  temp.unshift(events[i]);
                }
              }
            }
            $timeout(function() {
                $scope.calendar.eventSource = temp;
            }, 500);
            $scope.closeFilterEventsPopup();
        }

        $scope.closeFilterEventsPopup = function() {
            filterPopup.close();
        }

        $scope.showHelpPopup = function() {
          var helpPopup = $ionicPopup.alert({
            title: 'Calendar Help',
            template: 'Swipe left or right on the calendar to navigate between months, weeks, and days.' +
            '<br><br>' +
            '<div style="background-color: #000; color: #FFF; padding: 20px">' +
            '<span style="text-decoration: underline">Legend</span><br><br>' +
            '<span style="color: #11C1F3; font-weight: bold">Blue</span>: U.S. federal holiday<br>' +
            '<span style="color: #009900; font-weight: bold">Dark green</span>: Selected day<br>' +
            '<span style="color: #33CD5F; font-weight: bold">Green</span>: Islamic holiday<br>' +
            '<span style="color: #D1C9DA; font-weight: bold">Light purple</span>: Hindu holiday<br>' +
            '<span style="color: #886AEA; font-weight: bold">Violet</span>: Christian holiday<br><br>' +
            'M: Displays monthly view<br>' +
            'W: Displays weekly view<br>' +
            'D: Displays daily view<br><br>' +
            '<span class="glyphicon glyphicon-star-empty"></span>: Opens Add Event dialog<br>' +
            '<span class="glyphicon glyphicon-list"></span>: Opens Load Calendar dialog<br>' +
            '<span class="glyphicon glyphicon-minus-sign" style="color: #E44437"></span>: Opens Remove Events dialog<br>' +
            '<span class="glyphicon glyphicon-globe" style="color: #0275D8"></span>: Populates calendar with holidays<br>' +
            '<span class="glyphicon glyphicon-filter" style="color: #E3F2FD"></span>: Opens Filter Events dialog<br>' +
            '<span class="glyphicon glyphicon-floppy-disk" style="color: #33CD5F"></span>: Opens Save Calendar dialog<br>' +
            '<span class="glyphicon glyphicon-remove" style="color: #E44437"></span>: Opens Clear Calendar dialog' +
            '</div><br>' +
            'Questions? Comments?<br>Please e-mail BJ Peter DeLaCruz at ' +
            '<span style="text-decoration: underline">bj.peter.delacruz@gmail.com</span>.' +
            '</ul>'
          });
        }

        $scope.showLoadEventsPopup = function() {
            $http({
                method: 'GET',
                url: 'http://sleepy-badlands-60233.herokuapp.com/events/' + deviceID
            }).then(function successCallback(response) {
                showList(response.data);
            }, function errorCallback(response) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Error!',
                    template: "Failed to get events from server: " + response
                });
            });
        };

        $scope.removeEvent = function(e) {
          var confirmPopup = $ionicPopup.confirm({
              title: 'Remove Event',
              template: 'Are you sure you want to remove ' + e + ' from your calendar?'
          });
          confirmPopup.then(function(res) {
              if (res) {
                var temp = [];
                $scope.calendar.eventSource.forEach(function(o1) {
                  if (o1.title != e) {
                    temp.unshift(o1);
                  }
                });
                events = temp;
                $scope.calendar.eventSource = events;
                $scope.events = events;
                $localStorage.events = events;
                cordova.plugins.notification.local.cancel(notificationIds[e]);
                $scope.closeRemoveEventsPopup();
              }
          });
        }

        $scope.showSaveEventsPopup = function() {
            $ionicPopup.prompt({
                title: 'Save Calendar',
                template: 'Enter a name for your calendar.',
                inputType: 'text',
                inputPlaceholder: 'Name'
            }).then(function(res) {
                if (res) {
                    saveEvents(res, null);
                }
            });
        }

        function toJson(deviceID, name, e) {
            return '{'
              + '\"deviceID\":\"' + deviceID + '\",'
              + '\"name\":\"' + name + '\",'
              + '\"events\":' + angular.toJson(e)
              + '}';
        }

        function saveEvents(name, eventSource) {
            var e = eventSource ? eventSource : ($scope.calendar.eventSource ? $scope.calendar.eventSource : []);
            $.ajax({
                url: 'http://sleepy-badlands-60233.herokuapp.com/events',
                type: 'POST',
                contentType: 'application/json',
                data: toJson(deviceID, name, e),
                success: function(result) {
                    console.log("Success.");
                }
            });
        }

        $scope.clearEvents = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Clear Calendar',
                template: 'Are you sure you want to remove all the events from your calendar?'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    events = [];
                    $scope.calendar.eventSource = [];
                    $scope.events = [];
                    $localStorage.events = [];
                    cordova.plugins.notification.local.cancellAll();
                }
            });
        }

        function showList(data) {
            popupList = [];
            angular.forEach(data, function(item) {
                popupList.push({ 'Id': item._id, 'Text': item.name});
            });
            $scope.dataSource = popupList;
            listPopup = $ionicPopup.show({
                templateUrl: 'listTemplate.html',
                title: 'Load Calendar',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                ]
            });
        };

        $scope.tapped = function(eventsName) {
            loadEventsFromServer(eventsName);
            listPopup.close();
        };

        $scope.onEventSelected = function(event) {
            console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
        };

        $scope.onViewTitleChanged = function(title) {
            $scope.viewTitle = title;
        };

        $scope.today = function() {
            $scope.calendar.currentDate = new Date();
        };

        $scope.isToday = function() {
            var today = new Date();
            var currentCalendarDate = new Date($scope.calendar.currentDate);

            today.setHours(0, 0, 0, 0);
            currentCalendarDate.setHours(0, 0, 0, 0);
            return today.getTime() === currentCalendarDate.getTime();
        };

        $scope.onTimeSelected = function(selectedTime, events) {
            console.log('Selected time: ' + selectedTime + ', hasEvents: ' + (events !== undefined && events.length !== 0));
        };

        $scope.loadHolidays = function() {
            $http({
                method: 'GET',
                url: 'http://sleepy-badlands-60233.herokuapp.com/holidays'
            }).then(function successCallback(response) {
                if ($scope.calendar.eventSource) {
                    response.data.forEach(function(o1) {
                        var isFound = false;
                        $scope.calendar.eventSource.forEach(function(o2) {
                            if (o1.title === o2.title) {
                                isFound = true;
                            }
                        });
                        if (!isFound) {
                            $scope.calendar.eventSource.push(o1);
                        }
                    });
                } else {
                  $timeout(function() {
                      $scope.calendar.eventSource = response.data;
                      events = $scope.calendar.eventSource;
                      $scope.events = events;
                  }, 500);
                }
            }, function errorCallback(response) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Error!',
                    template: "Failed to get events from server: " + response
                });
            });
        }

        function loadEventsFromServer(name) {
            // JSON format: { title: "", startTime: "", endTime: "", allDay: true/false}
            $http({
                method: 'GET',
                url: 'http://sleepy-badlands-60233.herokuapp.com/events/' + deviceID + "/" + name
            }).then(function successCallback(response) {
                $scope.calendar.eventSource = response.data;
                events = $scope.calendar.eventSource;
                eventsName = name;
            }, function errorCallback(response) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Error!',
                    template: "Failed to get events from server: " + response
                });
            });
        }
        /*
        function createRandomEvents() {
            var events = [];
            for (var i = 0; i < 50; i += 1) {
                var date = new Date();
                var eventType = Math.floor(Math.random() * 2);
                var startDay = Math.floor(Math.random() * 90) - 45;
                var endDay = Math.floor(Math.random() * 2) + startDay;
                var startTime;
                var endTime;
                if (eventType === 0) {
                    startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                    if (endDay === startDay) {
                        endDay += 1;
                    }
                    endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                    events.push({
                        title: 'All Day - ' + i,
                        startTime: startTime,
                        endTime: endTime,
                        allDay: true
                    });
                } else {
                    var startMinute = Math.floor(Math.random() * 24 * 60);
                    var endMinute = Math.floor(Math.random() * 180) + startMinute;
                    startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                    endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                    events.push({
                        title: 'Event - ' + i,
                        startTime: startTime,
                        endTime: endTime,
                        allDay: false
                    });
                }
            }
            return events;
        }
        */
    });
